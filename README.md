# BinaryCoder

BinaryCoder helps you serialize Swift models to/from binary files, using the `Codable` protocol. To make things even easier, BinaryCoder includes two helpers:

1. A new protocol, `Countable`, which lets you indicate an association between an Array and a property which encodes the count (ie, entries and entryCount).
2. An integer wrapper type, `Counter`, which uses a closure when encoding the value. This allows you to have a counter property return the actual count of its associated array.